'''Walk Ultimate
written by Tijs Limburg
with help of online forums'''

import os

base_dir = "./"

print """Welcome to the walk ultimate program.  There are some initial steps you will need to take before continuing with the program.\n
     1. Please create a directory where you will store your collection.  Something like F:\collection will work.\n
     2. Please create a directory where you will store any duplicates.  Something like F:\dups will work.\n
     3. Run the program for each type of file.  You can specify the filetype in the next few steps.\n
     4. Leave plenty of time for the program to execute.  This may take a few hours depending on the size of your drive.\n
     4. Enjoy.  Please leave comments at tijis311@gmail.com"""

user = True
while user == True:     
     new_dir = raw_input(r"Please enter the full path where you want to store your collection: ")
     print "saving collection to " + new_dir
     dup = raw_input(r"Please enter the full path where you want to store your duplicates: ")
     print "saving dups to " + new_dir
     fType = raw_input(r"Please enter the file extension you would like to use: (eg .mp3) ")
     print "Searching for " + fType

     for t in os.walk(base_dir):
          for f in t[2]:
               if f[-4:] == ".mp3":
                    tempName = os.path.join(t[0], f)
                    for c in os.walk(new_dir):
                         for cn in c[2]:
                              if f == cn:
                                   exists = "true"
                                   break
                              else:
                                   exists = "false"
                              if exists == "false":
                                   print tempName
                                   os.rename(tempName, new_dir + "/" + f)
                              else:
                                   print "already exists"
                                   print tempName
                                   os.rename(tempName, dup + "/" + f)
                                   """
                                   for n in os.walk(dup):
                                        for dn in n[2]:
                                             if f == dn:
                                                  dExists = "true"
                                                  break
                                             else:
                                                  dExists = "false"
                                        if dExists == "false":
                                             os.rename(tempName, dup + "/" + f)
                                        else:
                                             print "Appending"
                                             append = "_1"
                                             os.rename(tempName, dup + "/" + f + append)
                                             """
     useAgain = raw_input("Would you like to move another filetype? (y or n): ")
     if useAgain == 'y':
          user = True
          continue
     else:
          user = False
